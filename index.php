<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Day04</title>
  <link rel="stylesheet" href='style.css' />
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
</head>

<body>
  <?php
    $error = array();
    $data = array();
    $reg = "/^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}$/";
    if (!empty($_GET['btnSubmit'])) {
      // Lấy dữ liệu
      $data['userName'] = isset($_GET['userName']) ? $_GET['userName'] : '';
      $data['gender'] = isset($_GET['gender']) ? $_GET['gender'] : '';
      $data['faculty'] = isset($_GET['faculty']) ? $_GET['faculty'] : '';
      $data['birthday'] = isset($_GET['birthday']) ? $_GET['birthday'] : '';

      // Kiểm tra định dạng dữ liệu
      if (empty($data['userName'])) {
        $error['userName'] = 'Hãy nhập tên.';
      }

      if (empty($data['gender'])) {
        $error['gender'] = 'Hãy chọn giới tính.';
      }
      if (empty($data['faculty'])) {
        $error['faculty'] = 'Hãy chọn phân khoa.';
      }
      if (empty($data['birthday'])) {
        $error['birthday'] = 'Hãy nhập ngày sinh.';
      }
      if (!preg_match($reg, $data['birthday'])) {
        $error['birthdayFormat'] = 'Hãy nhập ngày sinh đúng định dạng.';
      }
    }
  ?>
  <div class="container">
    <div class="form-group">
      <?php
        if ($error) {
          foreach ($error as $key => $value) {
            echo "<p class='error'> $value </p>";
          }
        }
      ?>
      <form action="index.php" method="GET" id="form">
        <div class="my-row-input">
          <label class="my-row-label">Họ và tên<span style="color: red"> *</span></label>

          <input type="text" name="userName" id="userName" class="input-text" size="30">
        </div>

        <div class="my-row-input">
          <label class="my-row-label">Giới tính<span style="color: red"> *</span></label>

          <div class="form-input-radio">
            <?php
            $gender = array('genderMale' => 'Nam', 'genderFemale' => 'Nữ');
            foreach ($gender as $key => $value) {
              echo "<input type='radio' id='$key' name='gender' value='$value'>";
              echo "<label for='$key' style='margin: 6px 6px 0px'>$value</label>";
            }
            ?>
          </div>
        </div>

        <div class="my-row-input">
          <label class="my-row-label">Phân khoa<span style="color: red"> *</span></label>

          <div class="form-input-select">
            <select id="faculty" name="faculty" id="faculty" style="height: inherit">
              <?php
                $faculty = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                foreach ($faculty as $key => $value) {
                  echo "<option value='$key'>$value</option>";
                }
              ?>
            </select>
            <div class="triangle"></div>
          </div>
        </div>

        <div class="my-row-input">
          <label class="my-row-label">Ngày sinh<span style="color: red"> *</span>
          </label>
          <input type="text" name="birthday" id="birthday" class="input-birthday" placeholder="dd/mm/yyyy">
        </div>

        <div class="my-row-input">
          <label class="my-row-label">Địa chỉ</label>
          <input type="text" name="address" id="address" class="input-text">
        </div>

        <div class="my-row-input-btn">
          <input type="submit" name="btnSubmit" id="btnSubmit" class="btn-submit" value="Đăng ký">
        </div>

      </form>
    </div>
  </div>

  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <!-- Bootstrap -->
  <!-- Bootstrap DatePicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <!-- Bootstrap DatePicker -->
  <script type="text/javascript">
    $(function() {
      $('#birthday').datepicker({
        format: "dd/mm/yyyy"
      });
    });
  </script>
</body>

</html>